package net.chenlin.dp.ids.server.manager;

/**
 * 配置管理模块
 * @author zhouchenglin[yczclcn@163.com]
 */
public interface ConfigManager {

    /**
     * 获取配置
     * @param key
     * @return
     */
    String getConfig(String key);

}
