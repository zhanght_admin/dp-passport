package net.chenlin.dp.ids.admin.dao;

import net.chenlin.dp.ids.admin.core.base.BaseMapper;
import net.chenlin.dp.ids.admin.entity.EmpUserDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * passport用户dao
 * @author zhouchenglin[yczclcn@163.com]
 */
@Mapper
public interface EmpUserMapper extends BaseMapper<EmpUserDO> {



}
